package clases;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Clase1 {
	public void metodo() throws ClassNotFoundException {
		String integrantes[] = new String[10];
		for(int i = 0; i<integrantes.length; i++) {
			integrantes[i] = "hola "+i;
		}
		try {
            try (ObjectOutputStream escritura = new ObjectOutputStream(new FileOutputStream("C:\\Users\\cass4\\Desktop\\UDEC\\PROGRAMACION II\\Eclipse\\prueba\\prueba.txt"))) {
                escritura.writeObject(integrantes);
                escritura.close();
            }
            try (ObjectInputStream lectura = new ObjectInputStream(new FileInputStream("C:\\Users\\cass4\\Desktop\\UDEC\\PROGRAMACION II\\Eclipse\\prueba\\prueba.txt"))) {
                String integrantesEscritura[] = (String[]) lectura.readObject();
                lectura.close();

                for (int i = 0; i < integrantesEscritura.length; i++) {
                    if (integrantesEscritura[i] != null) {
                        System.out.println((i + 1) + ". " + integrantesEscritura[i]);
                    }
                }
            }

        } catch (IOException e) {

        }
	}
}
